/*
 * Copyright (C) 2022  Lothar Ketterer
 *
 * This file is part of the app "rounds".
 *
 * rounds is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * rounds is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

Page {
    id: helpPage
    header: PageHeader {
        id: infoHeader
        title: i18n.tr('Help')
    }

    Flickable {
        id: contentFlick
        anchors.fill: parent
        topMargin: helpPage.header.flickable ? 0 : helpPage.header.height
        //contentHeight: units.gu(110)
        contentHeight: helpText.height + units.gu(10)

        Label {
            id: helpText
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                margins: units.gu(2)
            }
            text: i18n.tr('This app repeats a countdown of a defined length ' +
            '(via settings, section Action, ' +
            'item Length) for a certain number of cycles (section Action, ' +
            'item Rounds).\n\n' +
            'Unless the length of the breaks (section Breaks, item Length) ' +
            'is set to 0 min 0 secs, the rounds are separated by breaks of ' +
            'the defined length.\n\n' +
            'A delay can be set prior to the first round where you can, e.g., ' +
            'get into position for whatever your first action will be.\n\n' +
            'Note that the counter will NOT continue when the app is put into ' +
            'background. You can disable background suspension via ' +
            'UT Tweak Tool for the counter to continue in such a situation. ' +
            'This will make the battery drain faster.\n\n' +
            'Unless silent mode is enabled, sounds will be played at the start ' +
            'of each round, start of each break and after the last round ' +
            'has ended. The countdown setting for each section will ' +
            'generate a sound per second for the chosen number ' +
            'of last seconds of the corresponding item.\n\n' +
            'The sounds can be customized ' +
            '(%1 page, button \"%2\"). For more ' +
            'details regarding custom sounds, see the help page available ' +
            'on the sound selector ' +
            'page.\n\n').arg(i18n.tr('Settings')).arg(i18n.tr('Select sounds'))
            wrapMode: Text.Wrap
        }
    }
}
