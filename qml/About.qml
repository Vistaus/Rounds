/*
 * Copyright (C) 2022  Lothar Ketterer
 *
 * This file is part of the app "rounds".
 *
 * rounds is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * rounds is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3

Page {
    id: aboutPage
    header: PageHeader {
        id: aboutHeader
        title: i18n.tr('About')
    }

    Label {
        id: versionLabel
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: aboutHeader.bottom
            margins: units.gu(2)
        }
        text: '%1 v%2'.arg(root.appName).arg(root.version)
    }

    Label {
        id: copyleftLabel
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: versionLabel.bottom
            margins: units.gu(2)
        }
        text: '© 2022 Lothar Ketterer'
    }
    
    Label {
        id: licenseLabel
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: copyleftLabel.bottom
            margins: units.gu(2)
        }
        text: i18n.tr('License:') + ' <a href="https://codeberg.org/lk108/Rounds/LICENSE">GPLv3</a>'
        onLinkActivated: Qt.openUrlExternally('https://codeberg.org/lk108/Rounds/LICENSE')
    }

    Label {
        id: sourceLabel
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: licenseLabel.bottom
            topMargin: units.gu(2)
        }
        text: i18n.tr('Source code:')
    }

    Label {
        id: sourceLink
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: sourceLabel.bottom
        }
        text: '<a href="https://codeberg.org/lk108/Rounds">https://codeberg.org/lk108/Rounds</a>'
        onLinkActivated: Qt.openUrlExternally('https://codeberg.org/lk108/Rounds')
    }
    
    Label {
        id: bugsLabel
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: sourceLink.bottom
            topMargin: units.gu(2)
        }
        text: i18n.tr('Report bugs here:')
    }

    Label {
        id: bugsLink
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: bugsLabel.bottom
        }
        text: '<a href="https://codeberg.org/lk108/Rounds/issues">https://codeberg.org/lk108/Rounds/issues</a>'
        onLinkActivated: Qt.openUrlExternally('https://codeberg.org/lk108/Rounds/issues')
    }

    Label {
        id: creditLabel
        anchors {
            left: parent.left
            right: parent.right
            top: bugsLink.bottom
            margins: units.gu(2)
        }
        text: i18n.tr('Special thanks to the Thursday E. Team for the UBports Teach initiative and their help to start this project!\n\nBig thanks go to the contributors:')
        wrapMode: Text.Wrap
    }

    Label {
        id: contributorsLabel
        anchors {
            left: parent.left
            right: parent.right
            top: creditLabel.bottom
            leftMargin: units.gu(4)
            topMargin: units.gu(1)
        }
        text: 'walking-octopus\nAnne017\nHeimen Stoffels'
        wrapMode: Text.Wrap
    }
}
