/*
 * Copyright (C) 2022  Lothar Ketterer
 *
 * This file is part of the app "rounds".
 *
 * rounds is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * rounds is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

Page {
    id: helpPage
    header: PageHeader {
        id: infoHeader
        title: i18n.tr('Help for Sound select')
    }

    Flickable {
        id: contentFlick
        anchors.fill: parent
        topMargin: helpPage.header.flickable ? 0 : helpPage.header.height
        contentHeight: soundHelpText.height + units.gu(10)

        Label {
            id: soundHelpText
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                margins: units.gu(2)
            }
            text: i18n.tr('A sound is played at four different ' +
            'occasions: Start of an action round, start of a break, ' +
            'at finish and at each countdown second. ' +
            'Choose the occasion you want to set the sound for in the ' +
            'upper dropdown menu.\n\n' +
            'Via the lower dropdown, you can choose the location to ' +
            'select the sound from:\n\n' +
            '- User sounds: You can put sound files into the directory ' +
            '/home/phablet/.local/share/rounds.lotharketterer/sounds ' +
            '(you might have to create the last two directories first). ' +
            'The files can be any format that is supported by Ubuntu Touch, ' +
            'for example .mp3 or .ogg.\n\n' +
            '- App sounds: Sounds that are shipped with this app.\n\n' +
            '- System sounds: The notification sounds that come with Ubuntu Touch.\n\n' +
            'If you click on a sound, it will directly be pre-set for ' +
            'the respective occasion and finally set when you go back to ' +
            'the Settings page. While still on the sound selector page, ' +
            'you can reset the sound of the currently ' +
            'selected occasion to the setting before ' +
            'you entered the sound selector page by clickin on ' +
            'Undo (top right corner, second icon from right). To reset ' +
            'the sounds of all occasions, click ' +
            'Reset All (top right corner, right icon). ' +
            'Note that once you go back to the Settings page, the sounds ' +
            'have been selected and cannot be reset via the icons anymore.\n\n' +
            'For the general help page, click on the question mark icon on ' +
            'the main page or the Settings page.\n\n')
            wrapMode: Text.Wrap
        }
    }
}
