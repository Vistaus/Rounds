/*
 * Copyright (C) 2022  Lothar Ketterer
 *
 * This file is part of the app "rounds".
 *
 * rounds is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * rounds is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 * Code for the ListItems with the OptionSelector elements was taken from the
 * UBports notes app (Copyright: 2019 UBports, License: GPLv3), see
 * https://gitlab.com/ubports/apps/notes-app/-/blob/master/src/app/qml/ui/settings/ThemeSettings.qml
 * Compared to the notes app, the code has been modified; UBports is not
 * responsible for the behaviour of this app.
 *
 *
 *
 * A part of the code for the delegates (delegateBasename and
 * delegateWithExtension), especially the solution how to show a tick for the
 * selected sound only, was taken in a modified form from the timer app:
 * https://gitlab.com/Danfro/timer/-/blob/main/Timer/ui/SoundPickerPage.qml
 
 * The timer app is licensed under the BSD-3-Clause license which requires that
 * the following is reproduced here for this specific section of the code:
 * 
 * =========================================================================
 * LICENSE: BSD-3-Clause https://opensource.org/licenses/BSD-3-Clause
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 * 
 *     Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 * 
 *     Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * =========================================================================
 */

import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Qt.labs.folderlistmodel 2.1
import QtMultimedia 5.12
import Qt.labs.platform 1.1

Page {
    id: audioPage

    property string tempSound: ''
    property string tempRoundSound: ''
    property string tempBreakSound: ''
    property string tempFinishSound: ''
    property string tempCountdownSound: ''

    Component.onCompleted: {
        tempRoundSound = root.roundSoundPath
        tempBreakSound = root.breakSoundPath
        tempFinishSound = root.finishSoundPath
        tempCountdownSound = root.countdownSoundPath
        setTempSound()
    }

    Component.onDestruction: {
        if (tempAudio.PlaybackState == Audio.PlayingState) tempAudio.stop()
        setSpecificSound()
        root.roundSoundPath = tempRoundSound
        root.breakSoundPath = tempBreakSound
        root.finishSoundPath = tempFinishSound
        root.countdownSoundPath = tempCountdownSound
    }

    header: PageHeader {
        id: audioHeader
        title: i18n.tr('Select sounds')
        trailingActionBar.actions: [

//            // in case we want to switch to user confirmation of choices
//            Action {
//                iconName: 'ok'
//                text: i18n.tr('Confirm')
//                onTriggered: {
//                    if (tempAudio.PlaybackState == Audio.PlayingState) tempAudio.stop()
//                    setSpecificSound()
//                    root.roundSoundPath = tempRoundSound
//                    root.breakSoundPath = tempBreakSound
//                    root.finishSoundPath = tempFinishSound
//                    root.countdownSoundPath = tempCountdownSound
//                    layout.removePages(audioPage) 
//                }
//            },
            Action {
                iconName: 'reset'
                text: i18n.tr('Reset all')
                onTriggered: {
                    tempRoundSound = root.roundSoundPath
                    tempBreakSound = root.breakSoundPath
                    tempFinishSound = root.finishSoundPath
                    tempCountdownSound = root.countdownSoundPath
                    setTempSound()
                }
            },
            Action {
                iconName: 'edit-undo'
                text: i18n.tr('Undo')
                onTriggered: {
                    setTempSound()
                }
            },
            Action {
                iconName: 'help'
                text: i18n.tr('Help')
                onTriggered: {
                    layout.addPageToCurrentColumn(audioPage, Qt.resolvedUrl('SoundHelp.qml'))
                }
            }
        ]
    }

    function setSpecificSound() {
            if (root.selectedSoundOccasionIndex == Main.OccasionType.StartActionRound) {
                tempRoundSound = tempSound
            }
            else if (root.selectedSoundOccasionIndex == Main.OccasionType.StartBreak) {
                tempBreakSound = tempSound
            }
            else if (root.selectedSoundOccasionIndex == Main.OccasionType.Finish) {
                tempFinishSound = tempSound
            }
            else if (root.selectedSoundOccasionIndex == Main.OccasionType.Countdown) {
                tempCountdownSound = tempSound
            }
    }

    function setTempSound() {
            if (root.selectedSoundOccasionIndex == Main.OccasionType.StartActionRound) {
                tempSound = tempRoundSound
            }
            else if (root.selectedSoundOccasionIndex == Main.OccasionType.StartBreak) {
                tempSound = tempBreakSound 
            }
            else if (root.selectedSoundOccasionIndex == Main.OccasionType.Finish) {
                tempSound = tempFinishSound 
            }
            else if (root.selectedSoundOccasionIndex == Main.OccasionType.Countdown) {
                tempSound = tempCountdownSound 
            }
    }

    Column {
        width: parent.width    
        anchors {
            top: audioHeader.bottom
        }

        ListItem {
            id: soundOccasion
            height: occasionLayout.height 

            SlotsLayout {
                id: occasionLayout

//                // removed because we work directly with the index instead of the
//                // corresponding string. Reason is that if we use i18n,
//                // occasionSelector.model[i] will return the translated string
//                // instead of the standard one we use here in the code.
//                // This would still work fine if we check against the i18n text
//                // in the functions setTempSound() and setSpecificSound() above,
//                // but only if we do not switch between language settings
//                // because the previously selected occasion and directory
//                // are saved as Settings.
//                
//                function occasionIndex(occasionName) {
//                    for (var i = 0; i < occasionSelector.model.length; ++i) {
//                        if(occasionName == occasionSelector.model[i]) {
//                            return i
//                        }
//                    }
//                }

                mainSlot: OptionSelector {
                    id: occasionSelector
                    text: i18n.tr('Occasion to select sound for') 
                    containerHeight: itemHeight * 4
                    model: [i18n.tr('Start of action round'),
                            i18n.tr('Start of break'),
                            i18n.tr('Finish'),
                            i18n.tr('Countdown')]

                    onSelectedIndexChanged: {
                        setSpecificSound() // we have to do this before we change root.selectedSoundOccasionIndex
                        root.selectedSoundOccasionIndex = occasionSelector.selectedIndex
                        setTempSound()
                    }
   
                    Component.onCompleted: {
                        occasionSelector.selectedIndex = root.selectedSoundOccasionIndex
                    }
                }
            }
        }

        ListItem {
            id: soundDir
            height: soundDirLayout.height 

            SlotsLayout {
                id: soundDirLayout

                mainSlot: OptionSelector {
                    id: soundDirSelector
                    text: i18n.tr('Source of sounds')
                    containerHeight: itemHeight * 3
                    model: [i18n.tr('User sounds'),
                            i18n.tr('App sounds'),
                            i18n.tr('System sounds')]

                    onSelectedIndexChanged: {
                        var oldSoundDirIndex = root.selectedSoundDirIndex
                        root.selectedSoundDirIndex = soundDirSelector.selectedIndex
                        if (root.selectedSoundDirIndex == Main.SoundDirType.UserSounds) {
                            if (StandardPaths.locate(StandardPaths.DataLocation, 'sounds', StandardPaths.LocateDirectory) == '') {
                                PopupUtils.open(errorMessageNoUserDir)
                                root.selectedSoundDirIndex = oldSoundDirIndex
                                soundDirSelector.selectedIndex = root.selectedSoundDirIndex
                            }
                            else {
                                testListView.model = userDirModel
                                testListView.delegate = delegateWithExtension
                            } 
                        }
                        else if (root.selectedSoundDirIndex == Main.SoundDirType.AppSounds) {
                            testListView.model = appDirModel
                            testListView.delegate = delegateBasename
                        }
                        else if (root.selectedSoundDirIndex == Main.SoundDirType.SystemSounds) {
                            testListView.model = systemDirModel
                            testListView.delegate = delegateBasename
                        }
                    }
   
                    Component.onCompleted: {
                        if (root.selectedSoundDirIndex == Main.SoundDirType.UserSounds) {
                            if (StandardPaths.locate(StandardPaths.DataLocation, 'sounds', StandardPaths.LocateDirectory) == '') {
                                // set to App sounds if the directory for User sounds does not exist
                                root.selectedSoundDirIndex = Main.SoundDirType.AppSounds 
                            }
                        }
                        soundDirSelector.selectedIndex = root.selectedSoundDirIndex
                    }
                }
            }
        }

        FolderListModel {
            id: userDirModel
            nameFilters: [ '*' ]
            folder: StandardPaths.locate(StandardPaths.DataLocation, 'sounds', StandardPaths.LocateDirectory)
        }

        FolderListModel {
            id: appDirModel
            nameFilters: [ '*.ogg', '*.mp3', '*.oga' ]
            folder: Qt.resolvedUrl('../assets')
        }

        FolderListModel {
            id: systemDirModel
            nameFilters: [ '*.ogg', '*.mp3', '*.oga' ]
            folder: '/usr/share/sounds/ubports/notifications' 
        }

        ListView {
            id: testListView
            width: parent.width
            height: audioPage.height - (audioHeader.height + soundOccasion.height + soundDir.height )
            clip: true
            model: root.selectedSoundDirIndex == Main.SoundDirType.UserSounds ? userDirModel : (root.selectedSoundDirIndex == Main.SoundDirType.AppSounds ? appDirModel : systemDirModel)
            delegate: root.selectedSoundDirIndex == Main.SoundDirType.UserSounds ? delegateWithExtension : delegateBasename
        }

        Component {
            id: delegateBasename

            ListItem {
                id: testListItem
                // shall specify the height when Using ListItemLayout inside ListItem
                height: clickImplementedLayout.height // + (divider.visible ? divider.height : 0)
                divider.visible: false

                ListItemLayout {
                    id: clickImplementedLayout
                    title.text: fileBaseName

                    Icon {
                        name: "media-playback-pause"
                        color: clickImplementedLayout.title.color
                        width: units.gu(2)
                        height: width
                        SlotsLayout.position: SlotsLayout.Trailing
                        opacity: tempAudio.playbackState == Audio.PlayingState && (fileURL == tempSound) ? 1 : 0
                    }

                    Icon {
                        name: "tick"
                        color: clickImplementedLayout.title.color
                        width: units.gu(2)
                        height: width
                        SlotsLayout.position: SlotsLayout.Trailing
                        opacity: fileURL == tempSound ? 1 : 0
                    }
            }

            onClicked: {
                tempSound = fileURL

                if (tempAudio.playbackState == Audio.PlayingState){
                    tempAudio.stop()
                }
                else {
                    tempAudio.play()
                }
            }
        }
    }

    Component {
        id: delegateWithExtension

        ListItem {
            id: testListItem
            // shall specify the height when Using ListItemLayout inside ListItem
            height: clickImplementedLayout.height // + (divider.visible ? divider.height : 0)
            divider.visible: false

            ListItemLayout {
                id: clickImplementedLayout
                title.text: fileName
                Icon {
                    name: "media-playback-pause"
                    color: clickImplementedLayout.title.color
                    width: units.gu(2)
                    height: width
                    SlotsLayout.position: SlotsLayout.Trailing
                    opacity: (tempAudio.playbackState == Audio.PlayingState) && (fileURL == tempSound) ? 1 : 0
                }

                Icon {
                    name: "tick"
                    color: clickImplementedLayout.title.color
                    width: units.gu(2)
                    height: width
                    SlotsLayout.position: SlotsLayout.Trailing
                    opacity: fileURL == tempSound ? 1 : 0
                }
            }
            onClicked: {
                tempSound = fileURL

                if (tempAudio.playbackState == Audio.PlayingState){
                    tempAudio.stop()
                }
                else {
                    tempAudio.play()
                }
            }
        }
    }
}

    Audio {
        id: tempAudio
        source: tempSound
    }

    Component {
        id: errorMessageNoUserDir

        ErrorMessage { // see file ErrorMessage.qml
            title: i18n.tr('Error')
            text: i18n.tr('User directory does not exist. Click on Help for information how to set it up.')
        }
    }
}
