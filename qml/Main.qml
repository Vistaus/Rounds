/*
 * Copyright (C) 2022  Lothar Ketterer
 *
 * This file is part of the app "rounds".
 *
 * rounds is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * rounds is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import Ubuntu.Components.Popups 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtMultimedia 5.12
import Qt.labs.platform 1.1 // for StandardPaths.locate etc
import QtSystemInfo 5.0 // for ScreenSaver

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'rounds.lotharketterer'
    automaticOrientation: true
    anchorToKeyboard: true

    property string appName: i18n.tr('Rounds')
    property string version: '1.1.0' // todo: get this from the manifest
    property string oldVersion: '0'

    property string roundMinutesText: '';
    property string roundSecondsText: '';
    property string roundRepsText: '';
    property string roundCountdownText: '';

    property string breakMinutesText: '';
    property string breakSecondsText: '';
    property string breakCountdownText: '';

    property string startDelayText: '';
    property string startCountdownText: '';
    property bool silentMode: false;
    
    // Todo: Not using consistent names here: "action" is used synonymous to "round".
    // Also for actionMinutes, actionSeconds, actionCountdown and
    // actionReps below. These probably should have been called
    // roundActive, roundMinutes etc., but I'm too lazy to change this now.
    property bool actionActive: false; 

    property int currentSeconds: 1;
    property int currentCountdown: 1;
    property int temp: 0; // Todo: Couldn't figure out how to declare a local int variable for modulo calculation, so declared an int here

    property int actionMinutes: parseInt(root.roundMinutesText)
    property int actionSeconds: parseInt(root.roundSecondsText)
    property int actionCountdown: parseInt(root.roundCountdownText)
    property int actionReps: parseInt(root.roundRepsText)

    property int breakMinutes: parseInt(root.breakMinutesText)
    property int breakSeconds: parseInt(root.breakSecondsText)
    property int breakCountdown: parseInt(root.breakCountdownText)

    property int startDelaySeconds: parseInt(root.startDelayText)
    property int startCountdownSeconds: parseInt(root.startCountdownText)

    property bool firstRun: true;

    // has to be referenced with <filename>.<enum type>.<value>
    // e.g. Main.OccasionType.StartActionRound
    enum OccasionType {
        StartActionRound,
        StartBreak,
        Finish,
        Countdown
    }

    property int selectedSoundOccasionIndex: Main.OccasionType.StartActionRound;

    enum SoundDirType { 
        UserSounds,
        AppSounds,
        SystemSounds
    }

    property int selectedSoundDirIndex: Main.SoundDirType.UserSounds;

    property string roundSoundPath: (StandardPaths.locate(StandardPaths.DataLocation, 'sounds/action') == '') ? Qt.resolvedUrl('../assets/whistle-sound-shortened.mp3') : StandardPaths.locate(StandardPaths.DataLocation, 'sounds/action')
    property string breakSoundPath: (StandardPaths.locate(StandardPaths.DataLocation, 'sounds/break') == '') ? Qt.resolvedUrl('../assets/cruise-ship-horn-sound.mp3') : StandardPaths.locate(StandardPaths.DataLocation, 'sounds/break')
    property string finishSoundPath: (StandardPaths.locate(StandardPaths.DataLocation, 'sounds/finish') == '') ? Qt.resolvedUrl('../assets/Tada-sound.mp3') : StandardPaths.locate(StandardPaths.DataLocation, 'sounds/finish')
    property string countdownSoundPath: (StandardPaths.locate(StandardPaths.DataLocation, 'sounds/countdown') == '') ? Qt.resolvedUrl('../assets/bell.oga') : StandardPaths.locate(StandardPaths.DataLocation, 'sounds/countdown')

    Settings {
        id: settings
        property alias amt: root.roundMinutesText
        property alias ast: root.roundSecondsText
        property alias art: root.roundRepsText
        property alias act: root.roundCountdownText

        property alias bmt: root.breakMinutesText
        property alias bst: root.breakSecondsText
        property alias bct: root.breakCountdownText

        property alias sdt: root.startDelayText
        property alias sct: root.startCountdownText
        property alias sm: root.silentMode
        
        property alias fr: root.firstRun
        property alias ov: root.oldVersion

        property alias s_ssoi: root.selectedSoundOccasionIndex
        property alias s_ssdi: root.selectedSoundDirIndex

        property alias s_rsp: root.roundSoundPath
        property alias s_bsp: root.breakSoundPath
        property alias s_fsp: root.finishSoundPath
        property alias s_csp: root.countdownSoundPath
    }

    
    ScreenSaver {
        screenSaverEnabled: !(Qt.application.active && counterTimer.running)
    }

    AdaptivePageLayout {
        id: layout
        anchors.fill: parent
        layouts: [
            PageColumnsLayout {
                when: width > units.gu(100)
                // column #0
                PageColumn {
                    minimumWidth: units.gu(45)
                    preferredWidth: units.gu(70)
                    maximumWidth: units.gu(70)
                }
                // column #1 (only used as spacing)
                PageColumn {
                    fillWidth: true
                }
            },
            // only one column if width <= units.gu(100)
            PageColumnsLayout {
                when: true
                PageColumn {
                    fillWidth: true
                }
            }
        ]
        primaryPage: Page {

            anchors.fill: parent

            header: PageHeader {
                id: header
                title: '%1 v%2'.arg(root.appName).arg(root.version)
                    trailingActionBar.actions: [
                        Action {
                            iconName: 'settings'
                            text: i18n.tr('Settings')
                            onTriggered: {
                                if (!counterTimer.running && pauseButton.text == i18n.tr('Pause')) {
                                    layout.addPageToCurrentColumn(layout.primaryPage, Qt.resolvedUrl('Settings.qml'))
                                }
                            }
                        },

                        Action {
                            iconName: 'help'
                            text: i18n.tr('Help')
                            onTriggered: {
                                if (!counterTimer.running && pauseButton.text == i18n.tr('Pause')) {
                                    layout.addPageToCurrentColumn(layout.primaryPage, Qt.resolvedUrl('Help.qml'))
                                }
                            }
                        },

                        Action {
                            iconName: 'info'
                            text: i18n.tr('About')
                            onTriggered: {
                                if (!counterTimer.running && pauseButton.text == i18n.tr('Pause')) {
                                    layout.addPageToCurrentColumn(layout.primaryPage, Qt.resolvedUrl('About.qml'))
                                }
                            }
                        }
                    ]
            }


            Label {
                id: statusLabel
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    top: header.bottom
                    topMargin: units.gu(2)
                }
                text: i18n.tr('Press Start to begin')
                textSize: Label.Large
            }       

            Label {
               id: counterLabel
               anchors {
                   horizontalCenter: parent.horizontalCenter
                   top: statusLabel.bottom
               }
               text: 'xx:xx'
//             textSize: Label.XLarge // too small
               font.pixelSize: units.gu(15)
            }

            Label {
                id: nextLabel
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    top: counterLabel.bottom
                }
                text: i18n.tr('Up next:')
//              textSize: Label.Large
            }

            Label {
                id: upNextLabel
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    top: nextLabel.bottom
                }
                text: startButton.setUpNextLabelAtStart()
//              textSize: Label.Large // won't fit in one line on small screens with this textSize
            }

            Item {
                id: buttonCage
                width: units.gu(32)
                height: units.gu(4)
                
                anchors {
                    top: upNextLabel.bottom
                    topMargin: units.gu(2)
                    horizontalCenter: parent.horizontalCenter
                }

                // Starts the counter (or stops it, if it's running) 
                Button {
                    id: startButton
                    width: units.gu(14)
                    text: i18n.tr('Start!')
                    iconName: 'media-playback-start'
                    color: theme.palette.normal.positive
                    anchors {
                        top: buttonCage.top
                        verticalCenter: buttonCage.verticalCenter
                        left: buttonCage.left
                    }

                    function setUpNextLabelAtStart() {
                        var tempText = ''
                        if (startDelaySeconds > 0) {
                            tempText = i18n.tr('Round 1 after a delay of %1 secs').arg(startDelaySeconds)
                        }
                        else {
                            tempText = i18n.tr('Round 1 of %1, length: %2 min %3 sec').arg(parseInt(roundRepsText)).arg(actionMinutes).arg(actionSeconds)
                        }
                        return tempText
                    }

                    onClicked: {
                        // Check if we need to start or stop the sequence. When the Pause button is pressed, the timer is stopped, so checking
                        // for counterTimer.running alone is not enough
                        if (counterTimer.running || pauseButton.text == i18n.tr('Resume')) { 
                            counterTimer.stop()
                            actionAudio.stop()
                            breakAudio.stop()
                            finishAudio.stop()
                            countdownAudio.stop()
                            text = i18n.tr('Start!')
                            iconName = 'media-playback-start'
                            color = theme.palette.normal.positive
                            statusLabel.text = i18n.tr('Press Start to begin')
                            counterLabel.color = upNextLabel.color
                            counterLabel.text = 'xx:xx'
                            upNextLabel.text = setUpNextLabelAtStart()
                            pauseButton.text = i18n.tr('Pause')
                        }
                        else { // we're not in the middle of a sequence, so we start one now
                            actionAudio.stop()
                            breakAudio.stop()
                            finishAudio.stop()
                            countdownAudio.stop()

                            if (actionMinutes == 0 && actionSeconds == 0){ // Round time is 0, makes no sense to start. Go to settings instead.
                                PopupUtils.open(errorMessageZeroAction)
                                layout.addPageToCurrentColumn(layout.primaryPage, Qt.resolvedUrl('Settings.qml'))
                                    
                            }
                            else { // that's the beginning of an actual start

                            counterLabel.color = upNextLabel.color
                            actionReps = parseInt(roundRepsText)

                            if (startDelaySeconds > 0) {
                                currentSeconds = startDelaySeconds + 1
                                currentCountdown = startCountdownSeconds
                                actionActive = false;
                                statusLabel.text = i18n.tr('Prepare for first round')
                                upNextLabel.text = i18n.tr('Round 1 of %1, length: %2 min %3 sec').arg(parseInt(roundRepsText)).arg(actionMinutes).arg(actionSeconds)
                            }
                            else {
                                currentSeconds = actionMinutes * 60 + actionSeconds + 1
                                currentCountdown = actionCountdown
                                if (!silentMode) actionAudio.play()
                                actionActive = true;
                                statusLabel.text = i18n.tr('Round 1 of %1').arg(parseInt(roundRepsText))
                                // check what's up after the first round to set the upNextLabel
                                if (actionReps > 1) {
                                    if ((breakMinutes + breakSeconds) > 0) { // break time is nonzero, so there's a break after first round
                                        upNextLabel.text = i18n.tr('Break, length: %1 min %2 sec').arg(breakMinutes).arg(breakSeconds)
                                    }
                                    else { // no breaks in this setting, Round 2 will be next
                                        upNextLabel.text = i18n.tr('Round 2 of %1, length: %2 min %3 sec').arg(parseInt(roundRepsText)).arg(actionMinutes).arg(actionSeconds)
                                    }
                                }
                                else { // there's only one round, so it's diretly over after it
                                    upNextLabel.text = i18n.tr('The End')
                                }
                            }
                            text = i18n.tr('Stop')
                            iconName = 'media-playback-stop'
                            color = theme.palette.normal.negative
                            counterTimer.start()
                            }
                        }
                    }
                }

                Button {
                    id: pauseButton
                    width: units.gu(14)
                    text: i18n.tr('Pause')
                    iconName: 'media-playback-pause'
                    color: theme.palette.normal.focus
                    anchors {
                        top: buttonCage.top
                        verticalCenter: buttonCage.verticalCenter
                        right: buttonCage.right
                    }
                    onClicked: {
                        if (counterTimer.running) {
                            counterTimer.stop()
                            text = i18n.tr('Resume')
                        }
                        else if (text == i18n.tr('Resume') && startButton.text == i18n.tr('Stop')) {
                            counterTimer.start()
                            text = i18n.tr('Pause')
                        }
                    }
                }
            } // Rectangle buttonCage

            Timer { 
                id: counterTimer
                interval: 1000
                repeat: true

                function setCounterLabel() {
                    temp = currentSeconds / 60 ;
                    var counterLabelText = temp +  ':';
                    temp = currentSeconds % 60 ;
                    if (temp < 10) {
                        counterLabelText += '0' + temp ;
                    }
                    else {
                        counterLabelText += temp ;
                    }
                    counterLabel.text = counterLabelText;
                }

                onTriggered: {
                    currentSeconds -= 1;
                    if (countdownAudio.playbackState == Audio.PlayingState) countdownAudio.stop()
                    if (currentSeconds == 0){
                        if (actionActive) { // we just finished one round
                            if (actionAudio.playbackState == Audio.PlayingState) actionAudio.stop() // just in case the sound is longer than the round
                            actionReps -= 1
                                if (actionReps > 0) { // still some action rounds to come
                                    currentSeconds = breakMinutes * 60 + breakSeconds
                                    if (currentSeconds > 0) { // Breaks are enabled, start next break
                                        actionActive = false
                                        currentCountdown = breakCountdown
                                        statusLabel.text = i18n.tr('Break')
                                        upNextLabel.text = i18n.tr('Round %1 of %2, length: %3 min %4 sec').arg(((parseInt(roundRepsText) - actionReps) + 1)).arg(parseInt(roundRepsText)).arg(actionMinutes).arg(actionSeconds)
                                        if ( !silentMode ) breakAudio.play()
                                    }
                                    else { // Breaks are disabled, continue with next round
                                        currentSeconds = actionMinutes * 60 + actionSeconds
                                        currentCountdown = actionCountdown
                                        statusLabel.text = i18n.tr('Round %1 of %2').arg((parseInt(roundRepsText) - actionReps) + 1).arg(parseInt(roundRepsText))

                                        if (actionReps > 1) { // check if there are more rounds after the upcoming one to set tue upNextLabel
                                            upNextLabel.text = i18n.tr('Round %1 of %2, length: %3 min %4 sec').arg((parseInt(roundRepsText) - actionReps) + 2).arg(parseInt(roundRepsText)).arg(actionMinutes).arg(actionSeconds)
                                        }
                                        else { // no more rounds after this one, sequence will end
                                                upNextLabel.text = i18n.tr('The End')
                                        }
                                        if ( !silentMode ) {
                                            actionAudio.play()
                                        }
                                    }
                                }
                                else { // last action round just ended, stop the counter
                                    counterTimer.stop()
                                    if ( !silentMode ) finishAudio.play()
                                    counterLabel.text = 'xx:xx'
                                    counterLabel.color = upNextLabel.color // reset the text color in case it was red from a countdown. Don't know how to set it to "normal" so it will be ok for dark mode, too, so we take the color from another label
                                    statusLabel.text = i18n.tr('Finished!')
                                    upNextLabel.text = startButton.setUpNextLabelAtStart()
                                    startButton.text = i18n.tr('Start!')
                                    startButton.iconName = 'media-playback-start'
                                    startButton.color = theme.palette.normal.positive
                                    return
                                }
                            }
                            else { // we are coming from the start delay or a break, so we start an action round now
                                if (breakAudio.playbackState == Audio.PlayingState) breakAudio.stop() // just in case
                                if ( !silentMode ) actionAudio.play()
                                actionActive = true
                                currentSeconds = actionMinutes * 60 + actionSeconds
                                currentCountdown = actionCountdown
                                statusLabel.text = i18n.tr('Round %1 of %2').arg((parseInt(roundRepsText) - actionReps) + 1).arg(parseInt(roundRepsText))

                                // set the upNextLabel, for this we need to check if it's over after this round, and if not, whether a break or a new round will be next
                                if (actionReps > 1) { // it's not over after the upcoming round
                                    if ((breakMinutes + breakSeconds) > 0) { // break time is nonzero, so there's a break after the upcoming round
                                        upNextLabel.text = i18n.tr('Break, length: %1 min %2 sec').arg(breakMinutes).arg(breakSeconds)
                                    }
                                    else { // no breaks in this setting, a round will follow the round that we start now
                                        upNextLabel.text = i18n.tr('Round %1 of %2, length: %3 min %4 sec').arg((parseInt(roundRepsText) - actionReps) + 2).arg(parseInt(roundRepsText)).arg(actionMinutes).arg(actionSeconds)
                                    }
                                }
                                else { // there's no more rounds after the upcoming one
                                    upNextLabel.text = i18n.tr('The End')
                                }
                            }
                    }
                    if (currentSeconds <= currentCountdown){ 
                        if ( !silentMode ) countdownAudio.play() 
                        counterLabel.color = UbuntuColors.red
                    }
                    else {
                        counterLabel.color = upNextLabel.color
                    }
                    setCounterLabel()
                }
            }

            Audio {
                id: countdownAudio
                source: root.countdownSoundPath
            }

            Audio {
                id: actionAudio
                source: root.roundSoundPath
            }

            Audio {
                id: breakAudio
                source: root.breakSoundPath
            }

            Audio {
                id: finishAudio
                source: root.finishSoundPath
            }
        }
        Component.onCompleted: {
            if (oldVersion != version) {
                // if something needs to be done if version has changed,
                // do it here
                console.log('App version has changed from ',oldVersion,' to ',version)
                
                // then set oldVersion to version
                oldVersion = version
            }

            checkStartUp()

            // Debug todo
            console.log('root on completed(): root.roundSoundPath: ', root.roundSoundPath)
            console.log('root on completed(): root.breakSoundPath: ', root.breakSoundPath)
            console.log('root on completed(): root.finishSoundPath: ', root.finishSoundPath)
            console.log('root on completed(): root.countdownSoundPath: ', root.countdownSoundPath)
            console.log('root on completed(): settings for root.roundSoundPath: ', settings.value('s_rsp', 'nix da'))
            console.log('root on completed(): settings for root.breakSoundPath: ', settings.value('s_bsp', 'nix da'))
            console.log('root on completed(): settings for root.finishSoundPath: ', settings.value('s_fsp', 'nix da'))
            console.log('root on completed(): settings for root.countdownSoundPath: ', settings.value('s_csp', 'nix da'))
        }

        function checkStartUp() {
               if ((actionMinutes == 0 && actionSeconds == 0) || root.firstRun) {
                       layout.addPageToCurrentColumn(layout.primaryPage, Qt.resolvedUrl('Settings.qml'))
                }
        }
    }

    Component {
        id: errorMessageZeroAction

        ErrorMessage { // see file ErrorMessage.qml
            title: i18n.tr('Error')
            text: i18n.tr('Action length must be non-zero')
        }
    }
}
