/*
 * Copyright (C) 2022  Lothar Ketterer
 *
 * This file is part of the app "rounds".
 *
 * rounds is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * rounds is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3
import Ubuntu.Components.Popups 1.3
import Qt.labs.settings 1.0
import QtMultimedia 5.12

Page {
    id: settingsPage

    Component.onDestruction: {
        updateRootParameters()
    }

    function updateRootParameters() {

        // todo: check for trailing zeros (e.g. '05') in label texts and remove them

        root.roundMinutesText = (inputRoundTimeMin.text == '') ? '0' : inputRoundTimeMin.text
        root.roundSecondsText = (inputRoundTimeSec.text == '') ? '0' : inputRoundTimeSec.text
        root.roundRepsText = (inputRoundReps.text == '') ? '999' : inputRoundReps.text
        root.roundCountdownText = (inputRoundCountdown.text == '') ? '0' : inputRoundCountdown.text

        root.breakMinutesText = (inputBreakTimeMin.text == '') ? '0' : inputBreakTimeMin.text
        root.breakSecondsText = (inputBreakTimeSec.text == '') ? '0' : inputBreakTimeSec.text
        root.breakCountdownText = (inputBreakCountdown.text == '') ? '0' : inputBreakCountdown.text

        root.startDelayText = (inputStartDelay.text == '') ? '0' : inputStartDelay.text
        root.startCountdownText = (inputStartCountdown.text == '') ? '0' : inputStartCountdown.text
        root.silentMode = silentModeSwitch.checked

        root.firstRun = false

        if ((parseInt(root.roundMinutesText) + parseInt(root.roundSecondsText)) == 0) {
            PopupUtils.open(errorMessageZeroAction)
            layout.addPageToCurrentColumn(layout.primaryPage, Qt.resolvedUrl('Settings.qml'))
        }
        else {
            root.actionMinutes = parseInt(root.roundMinutesText)
            root.actionSeconds = parseInt(root.roundSecondsText)
            root.actionCountdown = parseInt(root.roundCountdownText)
            root.actionReps = parseInt(root.roundRepsText)

            root.breakMinutes = parseInt(root.breakMinutesText)
            root.breakSeconds = parseInt(root.breakSecondsText)
            root.breakCountdown = parseInt(root.breakCountdownText)

            root.startDelaySeconds = parseInt(root.startDelayText)
            root.startCountdownSeconds = parseInt(root.startCountdownText)
            upNextLabel.text = startButton.setUpNextLabelAtStart()
        }
    }

    Flickable {
        id: contentFlick
        anchors.fill: parent
        topMargin: settingsPage.header.flickable ? 0 : settingsPage.header.height
        contentHeight: units.gu(80)

        Label {
            id: roundHeader
            anchors {
                top: parent.top
                topMargin: units.gu(2)
                left: parent.left
                leftMargin: units.gu(1)
            }
            text: i18n.tr('Action')
        }

        Label {
            id: inputRoundTime
            anchors {
                verticalCenter: inputRoundTimeSec.verticalCenter
                left: parent.left
                leftMargin: units.gu(5)
            }
            text: i18n.tr('Length')
        }

        TextField {
            id: inputRoundTimeMin
            inputMethodHints: Qt.ImhDigitsOnly
            validator: IntValidator{bottom: 0; top: 99}
            width: units.gu(7)
            anchors {
                verticalCenter: inputRoundTimeSec.verticalCenter
                right: inputRoundTimeSeparator.left
                rightMargin: units.gu(1)
            }
            placeholderText: i18n.tr('min')
            text: root.roundMinutesText
            horizontalAlignment: TextInput.AlignRight
        }

        Label {
            id: inputRoundTimeSeparator
            anchors {
                verticalCenter: inputRoundTimeSec.verticalCenter
                right: inputRoundTimeSec.left
                rightMargin: units.gu(1)
            }
            text: ':'
        }

        TextField {
            id: inputRoundTimeSec
            inputMethodHints: Qt.ImhDigitsOnly
            validator: IntValidator{bottom: 0; top: 59}
            width: units.gu(7)
            anchors {
                top: roundHeader.bottom
                topMargin: units.gu(1)
                right: parent.right
                rightMargin: units.gu(1)
            }
            placeholderText: i18n.tr('sec')
            text: root.roundSecondsText
            horizontalAlignment: TextInput.AlignRight
        }

        Label {
            id: inputRoundRepsLabel
            anchors {
                verticalCenter: inputRoundReps.verticalCenter
                left: parent.left
                leftMargin: units.gu(5)
            }
            text: i18n.ctr('in the sense of \"number of rounds\"','Rounds')
        }

        TextField {
            id: inputRoundReps
            inputMethodHints: Qt.ImhDigitsOnly
            validator: IntValidator{bottom: 1; top: 999}
            width: units.gu(8)
            anchors {
                top: inputRoundTimeSec.bottom
                topMargin: units.gu(1)
                right: parent.right
                rightMargin: units.gu(1)
            }
            placeholderText: i18n.tr('max')
            text: root.roundRepsText
            horizontalAlignment: TextInput.AlignRight
        }

        Label {
            id: inputRoundCountdownLabel
            anchors {
                verticalCenter: inputRoundCountdown.verticalCenter
                left: parent.left
                leftMargin: units.gu(5)
            }
            text: i18n.tr('Countdown (seconds)')
        }

        TextField {
            id: inputRoundCountdown
            inputMethodHints: Qt.ImhDigitsOnly
            validator: IntValidator{bottom: 1; top: 10}
            width: units.gu(7)
            anchors {
                top: inputRoundReps.bottom
                topMargin: units.gu(1)
                right: parent.right
                rightMargin: units.gu(1)
            }
            placeholderText: i18n.tr('off')
            text: root.roundCountdownText
            horizontalAlignment: TextInput.AlignRight
        }
        
        Label {
            id: breakHeader
            anchors {
                top: inputRoundCountdown.bottom
                topMargin: units.gu(3)
                left: parent.left
                leftMargin: units.gu(1)
            }
            text: i18n.tr('Breaks')
        }

        Label {
            id: inputBreakTime
            anchors {
                verticalCenter: inputBreakTimeSec.verticalCenter
                left: parent.left
                leftMargin: units.gu(5)
            }
            text: i18n.tr('Length')
        }

        TextField {
            id: inputBreakTimeMin
            inputMethodHints: Qt.ImhDigitsOnly
            validator: IntValidator{bottom: 0; top: 99}
            width: units.gu(7)
            anchors {
                verticalCenter: inputBreakTimeSec.verticalCenter
                right: inputBreakTimeSeparator.left
                rightMargin: units.gu(1)
            }
            placeholderText: i18n.tr('min')
            text: root.breakMinutesText
            horizontalAlignment: TextInput.AlignRight
        }

        Label {
            id: inputBreakTimeSeparator
            anchors {
                verticalCenter: inputBreakTimeSec.verticalCenter
                right: inputBreakTimeSec.left
                rightMargin: units.gu(1)
            }
            text: ':'
        }

        TextField {
            id: inputBreakTimeSec
            inputMethodHints: Qt.ImhDigitsOnly
            validator: IntValidator{bottom: 0; top: 59}
            width: units.gu(7)
            anchors {
                top: breakHeader.bottom
                topMargin: units.gu(1)
                right: parent.right
                rightMargin: units.gu(1)
            }
            placeholderText: i18n.tr('sec')
            text: root.breakSecondsText
            horizontalAlignment: TextInput.AlignRight
        }


        Label {
            id: inputBreakCountdownLabel
            anchors {
                verticalCenter: inputBreakCountdown.verticalCenter
                left: parent.left
                leftMargin: units.gu(5)
            }
            text: i18n.tr('Countdown (seconds)')
        }

        TextField {
            id: inputBreakCountdown
            inputMethodHints: Qt.ImhDigitsOnly
            validator: IntValidator{bottom: 0; top: 10}
            width: units.gu(7)
            anchors {
                top: inputBreakTimeSec.bottom
                topMargin: units.gu(1)
                right: parent.right
                rightMargin: units.gu(1)
            }
            placeholderText: i18n.tr('off')
            text: root.breakCountdownText
            horizontalAlignment: TextInput.AlignRight
        }

        Label {
            id: miscHeader
            anchors {
                top: inputBreakCountdown.bottom
                topMargin: units.gu(3)
                left: parent.left
                leftMargin: units.gu(1)
            }
            text: i18n.tr('Misc')
        }


        Label {
            id: inputStartDelayLabel
            anchors {
                verticalCenter: inputStartDelay.verticalCenter
                left: parent.left
                leftMargin: units.gu(5)
            }
            text: i18n.tr('Delay to start (seconds)')
        }

        TextField {
            id: inputStartDelay
            inputMethodHints: Qt.ImhDigitsOnly
            validator: IntValidator{bottom: 1; top: 59}
            width: units.gu(7)
            anchors {
                top: miscHeader.bottom
                topMargin: units.gu(1)
                right: parent.right
                rightMargin: units.gu(1)
            }
            placeholderText: i18n.tr('off')
            text: root.startDelayText
            horizontalAlignment: TextInput.AlignRight
        }

        Label {
            id: inputStartCountdownLabel
            anchors {
                verticalCenter: inputStartCountdown.verticalCenter
                left: parent.left
                leftMargin: units.gu(5)
            }
            text: i18n.tr('Countdown to start (seconds)')
        }

        TextField {
            id: inputStartCountdown
            inputMethodHints: Qt.ImhDigitsOnly
            validator: IntValidator{bottom: 1; top: 10}
            width: units.gu(7)
            anchors {
                top: inputStartDelay.bottom
                topMargin: units.gu(1)
                right: parent.right
                rightMargin: units.gu(1)
            }
            placeholderText: i18n.tr('off')
            text: root.startCountdownText
            horizontalAlignment: TextInput.AlignRight
        }

        Label {
            id: silentModeLabel
            anchors {
                verticalCenter: silentModeSwitch.verticalCenter
                left: parent.left
                leftMargin: units.gu(5)
            }
            text: i18n.tr('Silent mode')
        }

        Switch {
            id: silentModeSwitch
            anchors {
                top: inputStartCountdown.bottom
                topMargin: units.gu(2)
                right: parent.right
                rightMargin: units.gu(1)
            }
            checked: root.silentMode
        }

        Button {
            id: soundSelectButton
            text: i18n.tr('Select sounds')
            anchors {
                top: silentModeLabel.bottom
                topMargin: units.gu(2)
                left: parent.left
                leftMargin: units.gu(5)
                right: parent.right
                rightMargin: units.gu(1)
            }
            onClicked: layout.addPageToCurrentColumn(settingsPage, Qt.resolvedUrl('SoundSelect.qml'))
        }
    }

    header: PageHeader {
        title: i18n.tr("Settings")
        flickable: layout.columns === 1 ? contentFlick : null
        // Show header when it gets locked in a two-column layout:
        onFlickableChanged: exposed = true


        trailingActionBar.numberOfSlots: 4
        trailingActionBar.actions: [
            Action {
                iconName: 'reset'
                text: i18n.tr('Reset all')
                onTriggered: {
                    inputRoundTimeMin.text = ''
                    inputRoundTimeSec.text = ''
                    inputRoundReps.text = ''
                    inputRoundCountdown.text = ''

                    inputBreakTimeMin.text = ''
                    inputBreakTimeSec.text = ''
                    inputBreakCountdown.text = ''

                    inputStartDelay.text = ''
                    inputStartCountdown.text = ''
                }
            },
            Action {
                iconName: 'edit-undo'
                text: i18n.tr('Undo')
                onTriggered: {
                    inputRoundTimeMin.text = root.roundMinutesText
                    inputRoundTimeSec.text = root.roundSecondsText
                    inputRoundReps.text = root.roundRepsText
                    inputRoundCountdown.text = root.roundCountdownText

                    inputBreakTimeMin.text = root.breakMinutesText
                    inputBreakTimeSec.text = root.breakSecondsText
                    inputBreakCountdown.text = root.breakCountdownText

                    inputStartDelay.text = root.startDelayText
                    inputStartCountdown.text = root.startCountdownText
                }
            },
            Action {
                iconName: 'help'
                text: i18n.tr('Help')
                onTriggered: {
                    layout.addPageToCurrentColumn(settingsPage, Qt.resolvedUrl('Help.qml'))
                }
            },
            Action {
                iconName: 'info'
                text: i18n.tr('About')
                onTriggered: {
                            layout.addPageToCurrentColumn(settingsPage, Qt.resolvedUrl('About.qml'))
                }
            }
        ]
    }
}
